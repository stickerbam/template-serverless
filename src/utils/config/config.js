  'use strict';
  
  /**
  * Main configuration schema
  * This defines what config values we have, what their type is and where to get them.
  * if an config value comes from environment variables (i.e. `env` value is set),
  * the definition must be added to the lambda env variables defined in to serverless.yml (under `environment`)
  *
  * We should have different .yml files for different stages. e.g. dev.yml, prod.yml
  * common.yml contains common configuration for all stages
  *
  * In case we want to use a config value in serverless.yml (e.g. a dynamodb table name), we can still define it here
  * like other values and refer to it like: ${file(./config/${env:DEPLOY_STAGE}.yml):myDynamoDbTableName}
  *
  * @type {convict}
  */

  const convict = require('convict');
  //  Reference configs so webpack can load them
  var common = require('./common.json');
  var stg = require('./staging.json');
  var prd = require('./production.json');

  // TODO: find a way of decrypt variables before returning them on get.
  convict.addFormat({
    name: 'kms-encrypted',
    validate: function(val) {
      if (!val || typeof(val) !== 'string') {
        throw new Error('KMS encripted value missing. Make sure all environment variables are properly set.');
      }
    }
  });

  let conf = convict({
    
    version: {
      doc: 'Application Version for simpler reference',
      default: '0.0.0',
      format: String
    },

    stage: {
      doc: 'The service stage (environment)',
      format: ['staging', 'production', 'test'],
      default: 'staging',
      env: 'DEPLOY_STAGE'
    },
    
    logLevel: {
      doc: 'The service stage (environment)',
      format: ['silly', 'verbose', 'debug', 'info', 'warn', 'error'],
      default: 'info',
      env: 'LOG_LEVEL'
    },

    repoName: {
      doc: 'Service repo name for tracking',
      default: 'default',
      format: String
    },

    alias: {
      doc: 'Alias in case it defers from repo name',
      default: 'serverless-authorizer',
      format: String
    },

    dynamodb: {
      assetsTable: {
        doc: 'Table that tracks all records uploaded by all users',
        default: null,
        format: String
      },
      filesTable: {
        doc: 'Table that points to every unique file saved in S3',
        default: null,
        format: String
      },
    },

    encryption: {
      jwtSecretKey: {
        doc: 'JWT Secret Key for user authentication purposes',
        default: 'MakeSureEnvVar-JWT_SECRET-IsSetProperly',
        format: 'kms-encrypted',
        env: 'JWT_SECRET'
      },
      jwtExpirationTime: {
        doc: 'JWT token expiration time',
        default: 600,
        format: 'int',
        env: 'JWT_EXPIRATION_TIME'
      },
      kmsKeyID: {
        doc: 'KMS key used to encrypt lambda env variables',
        default: 'MissingKMSKeyID',
        format: 'kms-encrypted',
      }
    }
  });

  // Initialize logger
  const Nfs = require('../lambda-logger/index');
  let nfs = Nfs.create(
    { name: conf.get('alias'), version: conf.get('version') }
  );
  
  try {
    conf.load(common);
    conf.load(conf.get('stage') === 'production' ? prd : stg);
    conf.validate({"allowed": "strict"});
  } catch (e) {
    nfs.logger.error(e);
  }

  module.exports = conf;
