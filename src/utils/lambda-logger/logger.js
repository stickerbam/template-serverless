'use strict';

var
  util = require('util'),
  SPACE = ' ',
  noop = function () {
  };

  
module.exports.create = function (appInfo, conf) {

  var levels = conf.logLevels,
    logLevel = appInfo.logLevel,
    logger = {level: logLevel},
    shouldLog = function (level) {
      return levels.indexOf(level) >= levels.indexOf(logLevel);
    };

  levels.forEach(function (level) {
    logger[level] = shouldLog(level) ? log : noop;

    function log() {
      var
        prefix = function () {
          return new Date().toISOString() + SPACE +
            appInfo.name + ':' + appInfo.version + SPACE +
            level.toUpperCase();
        };
      var normalizedLevel;

      switch (level) {
      case 'silly':
        normalizedLevel = 'info';
        break;
      case 'verbose':
        normalizedLevel = 'info';
        break;
      case 'debug':
        normalizedLevel = 'info';
        break;
      default:
        normalizedLevel = level;
      }

      prefix = prefix(level);

      if (typeof arguments[0] === 'object')
        arguments[0] = util.inspect(arguments[0], {depth: null}); //JSON.stringify(arguments[0], null, 2);

      arguments[0] = util.format(prefix, arguments[0]);

      var args = [].slice.apply(arguments);
      args = args.map(function (arg) {
        if (typeof arg === 'object')
          arg = util.inspect(arg, {depth: null}); //JSON.stringify(arg, null, 2);
        return arg;
      });

      console[normalizedLevel](util.format.apply(util, args)); // eslint-disable-line
    }

  });
  return logger;
};
