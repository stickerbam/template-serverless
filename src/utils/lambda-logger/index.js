'use strict';

const conf = require('./config');

module.exports.create = (applicationInfo, options) => {

  conf.load(options || {});
  conf.validate({allowed: 'strict'});

  const logger = require('./logger') .create(applicationInfo, conf);

  return {
    logger: logger
  };
};
