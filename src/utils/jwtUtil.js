const jwt = require('jsonwebtoken');
const uuidv1 = require('uuid/v1');
const crypto = require('crypto');

const conf = require('../utils/config/config');

const JWT_SECRET = conf.get('encryption.jwtSecretKey');
const JWT_EXPIRATION_TIME= conf.get('encryption.jwtExpirationTime');
const ANONYMOUS_TOKEN_EXPIRATION = JWT_EXPIRATION_TIME * 10;

function issueToken(payload) {
  return jwt.sign(payload, JWT_SECRET, { expiresIn: JWT_EXPIRATION_TIME });
}

function verifyToken(token) {
  return jwt.verify(token, JWT_SECRET);
}

function getAnonymousToken() {
  //generate token that last 1 year
  let id = createUserId(uuidv1(), 'guess');
  let payload = {
    userId: id,
    anonymousId: id,
    name: 'anonymous',
    scopes: ['*']
  };
  return jwt.sign(payload, JWT_SECRET, { expiresIn: ANONYMOUS_TOKEN_EXPIRATION});
}

function createUserId(data, secret) {
  const hmac = crypto.createHmac('sha256', secret);
  hmac.update(data);
  return hmac.digest('hex');
}

module.exports = {
  issueToken,
  verifyToken,
  getAnonymousToken
};